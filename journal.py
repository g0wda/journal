#!/usr/bin/python

from bottle import post, get, route, static_file, request, run
from json import dump, load, JSONEncoder

@route("/static/<path:path>")
def server_static(path=""):
    print "static file: %s" % path
    return static_file(path, root=".")


@get("/")
def index():
    return static_file("index.html", root=".")


@get("/journal.json")
def journal_json():
    return static_file("journal.json", root=".", mimetype="application/json")

PRETTY=True

@post("/journal")
def new_entry():

    entry = {}
    for key in ["text", "type", "datetime", "place", "lat", "lon"]:
        entry[key] = request.forms.get(key)

    f = open("journal.json")
    entries = load(f);
    f.close()

    entries.append(entry)

    f = open("journal.json", "w")
    if PRETTY:
        f.write(JSONEncoder(indent=2).encode(entries))
    else:
        dump(entries, f)
    f.close()

    return "true"

run(host="localhost", port=2222)
