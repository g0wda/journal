(function ($, markdown) {
  $(document).ready(function () {
    var $wrap = $("#journal"),
        template = $("#template").html(),
        last_date;

    function formatDate(d) {
        var dobj = new Date(d),
            months = ['Jan', 'Feb', 'March', 'April', 'May', 'June',
                      'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        function pad(t) {
            return t < 10 ? "0" + t : t;
        }

        return {
                date: months[dobj.getMonth()] + ' ' + dobj.getDate(),
                time: pad(dobj.getHours()) + ':' + pad(dobj.getMinutes())
               }
    }

    function transform(t, e) {
        var d = formatDate(e.datetime);

        e.date = d.date;
        e.time = d.time;

        e.text = markdown.toHTML(e.text)

        for (key in e) {
            if (e.hasOwnProperty(key)) {
                var regex = RegExp("{{" + key + "}}", "g");
                t = t.replace(regex, e[key]);
            }
        }
        return t;
    }

    function render($j, data) {
        var prev_date, e, d;
        $j.html("");
        for(var i=0, l=data.length; i<l; i++) {
            e=data[i], d=formatDate(e.datetime);
            e["class"] = prev_date == d.date ? "hide-date" : "";
            prev_date = last_date = d.date;

            $j.append(transform(template, e));
        }
        setTimeout(function () {
            window.scrollTo(0, $("#input-box").offset().top);
            $("#input-box").val(localStorage.draft_text).focus();
        }, 100);
    }
    function init(reinit) {
        $("#preview").html('');
        $.ajax({
            url: 'journal.json' + (reinit ? "?" + Math.random() : ""),
            type: 'GET',
            dataType: 'json',
            success: function(data) {
                render($wrap, data);
            },
            error: function(data, e) {
                alert("An error occured loading your data.");
                console.log("Error loading data: ", data, e);
            }
        });

    }

    function getData() {
        var data = {text: "", datetime: (new Date).toJSON(), type: "text", place: "", lat: "", lon:""};
        data.text = $("#input-box").val();
        data.place = $("#place").val();
        return data;
    }

    function preview(e) {
        var d = getData();
        if ($("#input-box").val().trim()) {
            d["class"] = (formatDate(d.datetime).date == last_date) ? "hide-date" : "";
            $("#preview").html(transform(template, d));
            window.scrollTo(0, $("#input-box").offset().top);
        } else {
            $("#preview").html("");
        }
    }

    $("#preview-btn").click(preview);
    $("#submit-btn").click(function () {
        var d = getData();
        if (d.text.trim() != "") {
            $.ajax({
                type: 'POST',
                url: '/journal',
                data: d,
                success: function () {
                    alert("Successfully added!");
                    localStorage.draft_text = "";
                    init(true);
                }
            });
        }
    });

    var live_preview, wait=0;
    $("#input-box").keyup(function () {
        if ($("#preview-flag").attr("checked")) {
            clearTimeout(live_preview);
            live_preview = setTimeout(preview, wait);
        }
        localStorage.draft_text = $(this).val();
    });

    $("#preview-flag").prop("checked", localStorage.live_preview)
        .change(function() {
            localStorage.live_preview = $(this).prop("checked");
        }
    );
    init();
  });
})(Zepto, markdown);
